<?php

namespace Drupal\angular_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'StarterBlock' block.
 *
 * @Block(
 *  id = "angular_starter_block",
 *  admin_label = @Translation("Angular Starter block"),
 * )
 */
class StarterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'app_name' => $this->t('myApp'),
      'controller_name' => $this->t('NameCtrl'),
      'interpolateprovider' => $this->t('~~'),
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['app_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Name'),
      '#description' => $this->t('This is This blocks App Name example ng-app=&quot;nameApp&quot; would just be nameApp'),
      '#default_value' => $this->configuration['app_name'],
      '#maxlength' => 200,
      '#size' => 64,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['controller_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('controller Name'),
      '#description' => $this->t('this is the controller Name example NameCtrl'),
      '#default_value' => $this->configuration['controller_name'],
      '#maxlength' => 200,
      '#size' => 64,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['interpolateprovider'] = [
      '#type' => 'textfield',
      '#title' => $this->t('interpolateProvider'),
      '#description' => $this->t('As angular uses {{var}} and so does twig we must set or angular vars to use a different symbol see <a href="https://groups.drupal.org/node/511061" target="_blank">https://groups.drupal.org/node/511061 </a>'),
      '#default_value' => $this->configuration['interpolateprovider'],
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['app_name'] = $form_state->getValue('app_name');
    $this->configuration['controller_name'] = $form_state->getValue('controller_name');
    $this->configuration['interpolateprovider'] = $form_state->getValue('interpolateprovider');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    //$build['angular_starter_block_app_name']['#markup'] = '<p>' . $this->configuration['app_name'] . '</p>';
    //$build['angular_starter_block_controller_name']['#markup'] = '<p>' . $this->configuration['controller_name'] . '</p>';
    //$build['angular_starter_block_interpolateprovider']['#markup'] = '<p>' . $this->configuration['interpolateprovider'] . '</p>';
    $build['#theme'] = 'angular_block';
    $build['#controller_name'] = $this->configuration['controller_name'];
    $build['#app_name'] = $this->configuration['app_name'];
    $build['#interpolateprovider'] = $this->configuration['interpolateprovider'];
    $build['#attached']['library'][] = 'angular_block/angular_lib';
    $build['#attached']['library'][] = 'angular_block/angular_block.config';
    $build['#attached']['drupalSettings']['angularBlock'] = [
      'appName' =>  $this->configuration['app_name'],
      'controllerName' => $this->configuration['controller_name'],
      'interpolateProvider' => $this->configuration['interpolateprovider'],
    ];
    // Disable cache.
    $build['#cache']['max-age'] = 0;
    return $build;
  }

}
