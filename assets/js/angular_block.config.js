/**
 * @file
 * This is the Js file set angular block config and interpolate provider.
 */

(function ($, drupalSettings) {
  'use strict';
  Drupal.behaviors.angularBlock = {
    attach: function (context, settings) {
      console.log(drupalSettings.angularBlock);
      var angularBlockSettings = drupalSettings.angularBlock;
      var appName = angularBlockSettings.appName;
      var controllerName = angularBlockSettings.controllerName;
      var drupalInterpolateProvider = angularBlockSettings.interpolateProvider;

      // Start Here , Hint view the html sourse of your block.
      var myApp = angular.module(appName, []);
      // Set the '{{}}' to not conflict with twig example '~~'.
      myApp.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol(drupalInterpolateProvider).endSymbol(drupalInterpolateProvider);
      });

      myApp.controller(controllerName, function ($scope) {
        $scope.firstName = 'fname';
        $scope.lastName = 'lname';
        // @todo cool stuffs here.
      });

    }
  };
})(jQuery, drupalSettings);
